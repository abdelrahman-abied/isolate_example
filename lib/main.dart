import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:flutter/material.dart';
import 'package:flutter_isolate/person.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:async/async.dart' show StreamGroup;

void main() {
  runApp(const MyApp());
}

// Future<Iterable<Person>> getPersons() async {
//   final rp = ReceivePort();
//   Isolate.spawn(_getPerson, rp.sendPort);
//   return await rp.first;
// }

// void _getPerson(SendPort sp) async {
//   const String url = "http://192.168.1.6:5500/apis/people1.json";
//   final persons = await HttpClient()
//       .getUrl(Uri.parse(url))
//       .then((req) => req.close())
//       .then((response) => response.transform(utf8.decoder).join())
//       .then((jsonString) => json.decode(jsonString) as List<dynamic>)
//       .then((json) => json.map((e) => Person.fromJson(e)));
//   // sp.send(persons);
//   Isolate.exit(sp, persons);
// }
// void _getMessages(SendPort sp) async {
//   await for (final now in Stream.periodic(
//       const Duration(seconds: 1), (_) => DateTime.now.toString()).take(10)) {
//     sp.send(now);
//   }
//   Isolate.exit(sp);
// }

// Stream<String> getMessages() {
//   final rp = ReceivePort();
//   return Isolate.spawn(_getMessages, rp.sendPort)
//       .asStream()
//       .asyncExpand((_) => rp)
//       .takeWhile((element) => element is String)
//       .cast();
// }

@immutable
class Request {
  final SendPort sendPort;
  final Uri uri;
  const Request(this.sendPort, this.uri);
  Request.from(PersonRequest request)
      : sendPort = request.receivePort.sendPort,
        uri = request.uri;
}

@immutable
class PersonRequest {
  final ReceivePort receivePort;
  final Uri uri;

  const PersonRequest(this.receivePort, this.uri);
  static Iterable<PersonRequest> all() sync* {
    for (var i in Iterable.generate(3, (i) => i)) {
      yield PersonRequest(
        ReceivePort(),
        Uri.parse("http://192.168.1.6:5500/apis/people${i + 1}.json"),
      );
    }
  }
}

Stream<Iterable<Person>> getPersons() {
  final streams = PersonRequest.all().map((req) =>
      Isolate.spawn(_getPersons, Request.from(req))
          .asStream()
          .asyncExpand((_) => req.receivePort)
          .takeWhile((element) => element is Iterable<Person>)
          .cast());
  return StreamGroup.merge(streams).cast();
}

void _getPersons(Request request) async {
  final persons = await HttpClient()
      .getUrl(request.uri)
      .then((req) => req.close())
      .then((response) => response.transform(utf8.decoder).join())
      .then((jsonString) => json.decode(jsonString) as List<dynamic>)
      .then((json) => json.map((e) => Person.fromJson(e)));

  Isolate.exit(request.sendPort, persons);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      supportedLocales: AppLocalizations.supportedLocales,
      locale: Locale("en", ''),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      // this method localeResolutionCallback return  device current locale
      localeResolutionCallback: (currentLocale, supportedLocales) {
        if (currentLocale != null) {
          print(currentLocale.languageCode);
          for (Locale locale in supportedLocales) {
            if (currentLocale.languageCode == locale.languageCode) {
              return currentLocale;
            }
          }
        }
        return supportedLocales.first;
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}

// void testIsolatedStream() async {
//   await for (final item in getMessages()) {
//     print(item);
//   }
// }
//
void testIt() async {
  await for (final msg in getPersons()) {
    print(msg);
  }
}

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                // final person = await getPersons();
                // debugPrint("$person");
                print("object");
                // testIsolatedStream();
              },
              child: Text(getString(context).click),
            ),
          ],
        ),
      ),
    );
  }

  AppLocalizations getString(BuildContext context) {
    return AppLocalizations.of(context)!;
  }
}

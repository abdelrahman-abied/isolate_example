// To parse this JSON data, do
//
//     final Person = PersonFromJson(jsonString);
//
// http://127.0.0.1:5500/apis/Person1.json
//

import 'dart:convert';

List<Person> PersonFromJson(String str) =>
    List<Person>.from(json.decode(str).map((x) => Person.fromJson(x)));

String PersonToJson(List<Person> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Person {
  String? name;
  int? age;

  Person({this.name, this.age});

  Person.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    age = json['age'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['age'] = age;
    return data;
  }

  @override
  String toString() => 'Person(name : $name, age:$age)';
}
